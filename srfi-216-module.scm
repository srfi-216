;;;; SPDX-FileCopyrightText: 2021 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: MIT

(module srfi-216
    (true false nil
     random
     runtime
     parallel-execute test-and-set!
     cons-stream stream-null? the-empty-stream)

  (import scheme)
  (import (chicken base))
  (import (chicken foreign))
  (import (chicken platform))
  (import (chicken random))
  (import (chicken time))
  (import (srfi 18))

  (register-feature! 'srfi-216)

  (include "srfi-216.scm"))

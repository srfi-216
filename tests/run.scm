;;;; SPDX-FileCopyrightText: 2021 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: MIT

(import scheme)
(import (chicken base))
(import (chicken random))
(import (chicken time))

(import (srfi 216))
(import test)

(define random-integer pseudo-random-integer)
(define current-second current-seconds)

(define-syntax check
  (syntax-rules (=>)
    ((_ expr => expected)
     (check expr equal? expected))
    ((_ expr equal expected)
     (test-assert (equal expr expected)))))

(include "srfi-216-tests.scm")
(test-exit)

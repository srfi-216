<!--
SPDX-FileCopyrightText: 2021 2021 Vasilij Schneidermann <mail@vasilij.de>

SPDX-License-Identifier: MIT
-->

## About

Port of the
[SRFI-216](https://srfi.schemers.org/srfi-216/srfi-216.html) (SICP
Prerequisites) reference implementation to CHICKEN.

## Docs

See [its wiki page].

[its wiki page]: https://wiki.call-cc.org/eggref/5/srfi-216
